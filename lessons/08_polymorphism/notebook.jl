### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ 1a1db1e2-2423-11eb-00fb-13e6681f0c2e
md"""
# Полиморфизм

Как было продемонстрировано, в Julia можно определить тип данных и соответствующие ему конструкторы и селекторы внутри модуля, что позволяет иметь в кодовой базе несколько альтернативных представлений абстрактных данных и легко выбрать одну из них через команду `using`. Но при этом нельзя использовать несколько реализаций совместно. Как можно заметить, Julia в принципе позволяет совместное использование нескольких представлений абстрактных данных хотя бы для чисел: математические процедуры корректно работают и с целыми числами, и с числами с плавающей точкой, и когда в выражении встречаются те и другие:
"""

# ╔═╡ ab10b5d4-2426-11eb-1190-ef68edd5bed7
cos(2)

# ╔═╡ b28bb6f6-2426-11eb-1223-dd819319fe72
cos(2.0)

# ╔═╡ b6998598-2426-11eb-3885-43de088fcd57
cos(2) == cos(2.0)

# ╔═╡ 1557044e-2cc3-11eb-12fb-bf82511db031
2^3

# ╔═╡ 24ccc7ba-2cc3-11eb-070e-d5c83084064a
2.0^3

# ╔═╡ 29360942-2cc3-11eb-207e-eb686b09352b
2^3.0

# ╔═╡ c07d2362-2426-11eb-2594-e76f035c08a6
md"""
Возможность процедур работать с различными представлениями данных называют *полиморфизмом*. Рассмотрим, как в Julia можно его реализовать.
"""

# ╔═╡ 5e6148ce-2427-11eb-0c8f-139d423ea534
md"""
## Типы данных

В Julia типы данных являются объектами первого класса, т.е. их можно использовать как значения - в частности, передавать как аргументы в процедуры или возвращать. Так, процедура `typeof` возвращает тип своего аргумента:
"""

# ╔═╡ d3077036-2427-11eb-37f1-819fe88a2de3
typeof(3.5)

# ╔═╡ 018b6f8e-2cc5-11eb-2096-e508dfaef9e0
typeof(3) == typeof(3.0)

# ╔═╡ 22d9aa86-2ccf-11eb-1c1d-732a5b2743d1
md"""
## Явные проверки типа для реализации полиморфизма

Вспомним два разных определения структур для представления величин, известных с погрешностью:

```julia
struct MeasSpan
	minval
	maxval
end

struct MeasAveUncert
	ave
	rel_uncert
end
```

Для абстрактного представления предлагается следующая реализация арифметических действий:
```julia
function add_meas(a, b)
    let mean_a = meanval(a), uabs_a = abs_uncert(a),
        mean_b = meanval(b), uabs_b = abs_uncert(b),
        mean_c = mean_a + mean_b, uabs_c = uabs_a + uabs_b
        return meas_from_ave_abs(mean_c, uabs_c)
    end
end

function sub_meas(a, b)
    let mean_a = meanval(a), uabs_a = abs_uncert(a),
        mean_b = meanval(b), uabs_b = abs_uncert(b),
        mean_c = mean_a - mean_b, uabs_c = uabs_a + uabs_b
        return meas_from_ave_abs(mean_c, uabs_c)
    end
end

function mul_meas(a, b)
    let mean_a = meanval(a), urel_a = rel_uncert(a),
        mean_b = meanval(b), urel_b = rel_uncert(b),
        mean_c = mean_a * mean_b, urel_c = urel_a + urel_b
        return meas_from_ave_rel(mean_c, urel_c)
    end
end

function div_meas(a, b)
    let mean_a = meanval(a), urel_a = rel_uncert(a),
        mean_b = meanval(b), urel_b = rel_uncert(b),
        mean_c = mean_a / mean_b, urel_c = urel_a + urel_b
        return meas_from_ave_rel(mean_c, urel_c)
    end
end
```
Чтобы использовать оба конкретных представления вместе, можно, к примеру, договориться, что конструктор `meas_from_ave_abs` будет создавать структуру первого типа, а `meas_from_mean_rel` - второго типа.
```julia
meas_from_ave_abs(ave, uabs) = MeasSpan(ave - uabs, ave + uabs)
meas_from_ave_rel(ave, urel) = MeasAveUncert(ave, urel)
```

Корректность селекторов `meanval`, `abs_uncert` и `rel_uncert` можно обеспечить через явную проверку типа:
```julia
function meanval(meas)
	let meastype = typeof(meas)
		if meastype == MeasSpan
			return (meas.maxval + meas.minval) / 2
		elseif meastype == MeasAveUncert
			return meas.ave
		else
			error("Unknown type for `meanval`: $meastype")
		end
	end
end

function abs_uncert(meas)
	let meastype = typeof(meas)
		if meastype == MeasSpan
			return (meas.maxval - meas.minval) / 2
		elseif meastype == MeasAveUncert
			return meanval(meas) * rel_uncert(meas)
		else
			error("Unknown type for `abs_uncert`: $meastype")
		end
	end
end

function rel_uncert(meas)
	let meastype = typeof(meas)
		if meastype == MeasSpan
			return ans_uncert(meas) / meanval(meas)
		elseif meastype == MeasAveUncert
			return meas.rel_uncert
		else
			error("Unknown type for `rel_uncert`: $meastype")
		end
	end
end
```

В этом случае введённые абстрактные определения для арифметики с неточными величинами будут работать корректно.


## Аддитивность и диспетчеризация

Техника вызова подходящей процедуры в зависимости от некоторого свойства аргумента называется *диспетчеризацией*, и представленные селекторы реализуют простейшую диспетчеризацию по типу аргумента.

Одной из проблем показанной реализации является то, что код селекторов должен "знать" обо всех конкретных реализациях абстрактных данных. Таким образом, добавление типа данных в систему требует внесения изменений в селекторы.

В таком случае говорят, что эта реализация полиморфизма не обладает свойством *аддитивности*. Под аддитивностью понимают ситуацию, когда для добавления в программу некоторой новой возможности не требуется изменять уже существующую часть программы.

К счастью, в Julia встроен мощный механизм диспетчеризации по типам аргументов, обеспечивающий эту аддитивность. К примеру, определение селектора `meanval` с индивидуальными реализациями для типов `MeasSpan` и `MeasAveUncert` "идиоматично" записывается следующим образом:
```julia
meanval(meas::MeasSpan) = (meas.maxval + meas.minval) / 2
meanval(meas::MeasAveUncert) = meas.ave
```

Запись
```julia
function fn(x::Type)
	...
end
```
означает, что эта версия процедуры `fn` будет применяться *только* для аргумента, имеющего тип `Type`. Говорят, что в этом случае создаётся *метод* процедуры `fn`. Если есть несколько методов для процедуры `fn`, то для конкретного аргумента (или набора аргументов) выбирается тот метод из подходящих, который накладывает наиболее строгие ограничения на типы аргументов.

Например, если определены методы
```julia
meanval(x) = x

meanval(meas::MeasSpan) = (meas.maxval + meas.minval) / 2
```
то для `meanval(MeasSpan(9, 10))` будет применяться второй метод как более ограничивающий, а для `meanval(10)` - первый, т.к. второй метод накладывает ограничения, которым аргумент не удовлетворяет.
"""

# ╔═╡ d9309352-2427-11eb-1899-e7129d27dcaf
md"""
### Иерархия типов

Определённые выше типы данных `MeasSpan` и `MeasAveUncert` никаким образом не объединены, кроме того, что для них определены методы процедур `meanval`, `abs_uncert` и `rel_uncert`. В ряде случаев целесообразно показать связь типов данных более явно. Язык Julia позволяет создавать *абстрактные типы* - "контейнеры", в которые можно добавлять другие абстрактные типы или конкретные реализации по мере необходимости.

Чтобы показать, что новый тип является подтипом некоторого абстрактного типа, используется запись
```julia
struct NewType <: AbstractType
	...
end
```
Каждый тип является подтипом только одного абстрактного типа. По умолчанию все новые типы являются подтипом "корневого" типа `Any`.

Абстрактный тип данных определяется следующей записью:
```julia
abstract type AType end
```

У абстрактных типов в Julia не может быть полей данных, они существуют только для добавления подтипов. С другой стороны, у *конкретных* типов (определяемых через `struct`) не может быть подтипов. В этом отличие иерархии типов Julia от иерархии наследования в объектно-ориентированных языках.

Часто для диспетчеризации по типам в определении метода не нужно указывать конкретный тип аргумента, достаточно указать абстрактный тип. К примеру, все числа в Julia относятся к типу `Number`, а все типы действительных чисел являются подтипами абстрактного типа `Real`.

С применением абстрактных типов мы можем определить операции над результатами измерений, которые включают возможность умножения результата измерения на число.

Также исправим небольшой недостаток базовой реализации: если оба аргумента для `add_meas`, `sub_meas` и т.д. имеют одинаковый тип, то сделаем и возвращаемый результат того же типа.
"""

# ╔═╡ 56a6151a-2ce2-11eb-0ad2-c16980d5210c
begin
	abstract type Measurement end
	
	struct MeasSpan <: Measurement
		minval
		maxval
	end

	struct MeasAveUncert <: Measurement
		ave
		rel_uncert
	end

	# Конструкторы
	meas_from_ave_abs(ave, uabs) = MeasSpan(ave - uabs, ave + uabs)
	meas_from_ave_rel(ave, urel) = MeasAveUncert(ave, urel)

	# Селекторы
	meanval(meas::MeasSpan) = (meas.maxval + meas.minval) / 2
	meanval(meas::MeasAveUncert) = meas.ave

	abs_uncert(meas::MeasSpan) = (meas.maxval - meas.minval) / 2
	abs_uncert(meas::MeasAveUncert) = meanval(meas) * rel_uncert(meas)
	
	rel_uncert(meas::MeasSpan) = abs_uncert(meas) / meanval(meas)
	rel_uncert(meas::MeasAveUncert) = meas.rel_uncert

	# Арифметика в абстрактном виде
	function add_meas(a, b)
		let mean_a = meanval(a), uabs_a = abs_uncert(a),
			mean_b = meanval(b), uabs_b = abs_uncert(b),
			mean_c = mean_a + mean_b, uabs_c = uabs_a + uabs_b
			return meas_from_ave_abs(mean_c, uabs_c)
		end
	end

	function sub_meas(a, b)
		let mean_a = meanval(a), uabs_a = abs_uncert(a),
			mean_b = meanval(b), uabs_b = abs_uncert(b),
			mean_c = mean_a - mean_b, uabs_c = uabs_a + uabs_b
			return meas_from_ave_abs(mean_c, uabs_c)
		end
	end

	function mul_meas(a, b)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_b = meanval(b), urel_b = rel_uncert(b),
			mean_c = mean_a * mean_b, urel_c = urel_a + urel_b
			return meas_from_ave_rel(mean_c, urel_c)
		end
	end
	
	function div_meas(a, b)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_b = meanval(b), urel_b = rel_uncert(b),
			mean_c = mean_a / mean_b, urel_c = urel_a + urel_b
			return meas_from_ave_rel(mean_c, urel_c)
		end
	end

	#=
	Дополнительные определения, чтобы `add_meas` и т.д. с двумя аргументами
	одного и того же типа сохраняли этот тип
	=#
	function add_meas(a::MeasAveUncert, b::MeasAveUncert)
		let mean_a = meanval(a), uabs_a = abs_uncert(a),
			mean_b = meanval(b), uabs_b = abs_uncert(b),
			mean_c = mean_a + mean_b, uabs_c = uabs_a + uabs_b
			return meas_from_ave_rel(mean_c, uabs_c / mean_c)
		end
	end

	function sub_meas(a::MeasAveUncert, b::MeasAveUncert)
		let mean_a = meanval(a), uabs_a = abs_uncert(a),
			mean_b = meanval(b), uabs_b = abs_uncert(b),
			mean_c = mean_a - mean_b, uabs_c = uabs_a + uabs_b
			return meas_from_ave_rel(mean_c, uabs_c / mean_c)
		end
	end

	function mul_meas(a::MeasSpan, b::MeasSpan)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_b = meanval(b), urel_b = rel_uncert(b),
			mean_c = mean_a * mean_b, urel_c = urel_a + urel_b
			return meas_from_ave_abs(mean_c, urel_c * mean_c)
		end
	end

	function div_meas(a::MeasSpan, b::MeasSpan)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_b = meanval(b), urel_b = rel_uncert(b),
			mean_c = mean_a / mean_b, urel_c = urel_a + urel_b
			return meas_from_ave_abs(mean_c, urel_c * mean_c)
		end
	end
	
	# Умножение неточной величины на число
	function mul_meas(a::MeasSpan, b::Real)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_c = mean_a * b, urel_c = urel_a
			return meas_from_ave_abs(mean_c, urel_c * mean_c)
		end
	end
	
	function mul_meas(a::MeasAveUncert, b::Real)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_c = mean_a * b, urel_c = urel_a
			return meas_from_ave_rel(mean_c, urel_c)
		end
	end
	
	mul_meas(b::Real, a) = mul_meas(a, b)
	
	div_meas(a, b::Real) = mul_meas(a, 1/b)

	function div_meas(b::Real, a::MeasSpan)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_c = b / mean_a, urel_c = urel_a
			return meas_from_ave_abs(mean_c, urel_c * mean_c)
		end
	end
	
	function div_meas(b::Real, a::MeasAveUncert)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_c = b / mean_a, urel_c = urel_a
			return meas_from_ave_rel(mean_c, urel_c)
		end
	end
end

# ╔═╡ 2ce0ef5e-2ce5-11eb-3978-ef4694d0203e
mass = meas_from_ave_abs(10, 0.01)

# ╔═╡ 3a672724-2ce5-11eb-2a54-6dc365a304cf
speed = meas_from_ave_abs(5, 0.2)

# ╔═╡ 5ae67ed2-2ce5-11eb-178a-e7fb47921ca9
kinetic_energy = div_meas(mul_meas(mass, mul_meas(speed, speed)), 2)

# ╔═╡ 9dc99964-2ce5-11eb-1c85-1f9464d2044e
md"""
## Расширение стандартных процедур

Удобство системы диспетчеризации в Julia состоит в том, что методы можно добавить к любой процедуре, даже к "стандартной". Т.е. мы можем при необходимости просто добавить методы к встроенным процедурам `Base.:+`, `Base.:-`, `Base.:*` и `Base.:/`.
"""

# ╔═╡ fcc0db12-2ce5-11eb-1487-f5ae5679f140
begin	
	Base.:+(a::Measurement, b::Measurement) = add_meas(a, b)
	Base.:-(a::Measurement, b::Measurement) = sub_meas(a, b)
	Base.:*(a::Measurement, b::Measurement) = mul_meas(a, b)
	Base.:/(a::Measurement, b::Measurement) = div_meas(a, b)
	
	Base.:*(a::Measurement, b::Real) = mul_meas(a, b)
	Base.:*(b::Real, a::Measurement) = a * b
	Base.:/(a::Measurement, b::Real) = div_meas(a, b)
	Base.:/(a::Real, b::Measurement) = div_meas(a, b)
end

# ╔═╡ 00032b48-2ce8-11eb-0397-cb8a7e5f67df
md"""
Добавив эти определения, мы можем работать с неточными величинами с таким же удобством, как с обычными числами.
"""

# ╔═╡ 94add1a4-2ce7-11eb-3c3e-edea8ef41422
ke = mass * speed * speed / 2

# ╔═╡ a7d15cc4-2ce7-11eb-3447-a3f48b2e9826
r1 = meas_from_ave_rel(2, 0.01)

# ╔═╡ dc4a37c6-2ce7-11eb-3e90-857890a4815c
r2 = meas_from_ave_rel(2, 0.01)

# ╔═╡ e4d006fc-2ce7-11eb-1799-add2686ec4b3
r_parallel = 1 / (1/r1 + 1/r2)

# ╔═╡ Cell order:
# ╟─1a1db1e2-2423-11eb-00fb-13e6681f0c2e
# ╠═ab10b5d4-2426-11eb-1190-ef68edd5bed7
# ╠═b28bb6f6-2426-11eb-1223-dd819319fe72
# ╠═b6998598-2426-11eb-3885-43de088fcd57
# ╠═1557044e-2cc3-11eb-12fb-bf82511db031
# ╠═24ccc7ba-2cc3-11eb-070e-d5c83084064a
# ╠═29360942-2cc3-11eb-207e-eb686b09352b
# ╟─c07d2362-2426-11eb-2594-e76f035c08a6
# ╟─5e6148ce-2427-11eb-0c8f-139d423ea534
# ╠═d3077036-2427-11eb-37f1-819fe88a2de3
# ╠═018b6f8e-2cc5-11eb-2096-e508dfaef9e0
# ╟─22d9aa86-2ccf-11eb-1c1d-732a5b2743d1
# ╟─d9309352-2427-11eb-1899-e7129d27dcaf
# ╠═56a6151a-2ce2-11eb-0ad2-c16980d5210c
# ╠═2ce0ef5e-2ce5-11eb-3978-ef4694d0203e
# ╠═3a672724-2ce5-11eb-2a54-6dc365a304cf
# ╠═5ae67ed2-2ce5-11eb-178a-e7fb47921ca9
# ╟─9dc99964-2ce5-11eb-1c85-1f9464d2044e
# ╠═fcc0db12-2ce5-11eb-1487-f5ae5679f140
# ╟─00032b48-2ce8-11eb-0397-cb8a7e5f67df
# ╠═94add1a4-2ce7-11eb-3c3e-edea8ef41422
# ╠═a7d15cc4-2ce7-11eb-3447-a3f48b2e9826
# ╠═dc4a37c6-2ce7-11eb-3e90-857890a4815c
# ╠═e4d006fc-2ce7-11eb-1799-add2686ec4b3
