### A Pluto.jl notebook ###
# v0.12.11

using Markdown
using InteractiveUtils

# ╔═╡ b3516a6e-25d8-11eb-1a72-c37f17a6be76
using .MeasSpan: Measurement, meanval, abs_uncert, rel_uncert, meas_from_ave_abs, meas_from_ave_rel

# ╔═╡ aacbf2f8-252c-11eb-099e-35718cca5ebb
md"""
# Альтернативные представления абстрактных данных

Рассмотренная ранее реализация арифметических операций для рациональных чисел реализует слой абстракции между арифметическими действиями и представлением чисел в памяти. Более сильной абстракцией является такая, которая может работать сразу с несколькими возможными представлениями данных. Для примера рассмотрим представление результатов измерений некоторой физической величины.

Измерение любой величины производится с некоторой погрешностью (uncertainty). Погрешность может быть либо абсолютной (R = (10.0±0.1) Ом), либо относительной (R = 10.0 Ом ± 1%).

Если по измеренным значениям нужно определить некоторую производную величину (например: известны падение напряжения $\Delta U$ на проводнике и ток $I$ через него, тогда сопротивление равно $R = \Delta U / I$), то такая величина также будет определена с погрешностью.

Для производной величины, являющейся суммой или разностью двух величин, *абсолютная* погрешность равна сумме *абсолютных* погрешностей суммируемых величин:
```math
\begin{align*}
A &= A_0 \pm ΔA \\
B &= B_0 \pm ΔB \\
S &= A + B \\
S_{min} &= (A_0 - \Delta A) + (B_0 - \Delta B) = (A_0 + B_0) - (\Delta A + \Delta B) \\
S_{max} &= (A_0 + \Delta A) + (B_0 + \Delta B) = (A_0 + B_0) + (\Delta A + \Delta B) \\
S &= (A_0 + B_0) \pm (\Delta A + \Delta B)
\end{align*}
```
Для производной величины, являющейся произведением или частным двух величин, *относительная* погрешность равна сумме *относительных* погрешностей первичных величин:
```math
\begin{align*}
P &= A \cdot B \\
P_{min} &= (A_0 - \Delta A) \cdot (B_0 - \Delta B) = A_0 \cdot B_0 - \Delta A \cdot B_0 - \Delta B \cdot A_0 + \Delta A \Delta B \\

\frac{P_{min}}{P_0} &= 1 - \left(\frac{\Delta A}{A_0} + \frac{\Delta B}{B_0}\right) + \frac{\Delta A}{A_0} \cdot \frac{\Delta B}{B_0} \approx 1 - \left(\frac{\Delta A}{A_0} + \frac{\Delta B}{B_0}\right) \\

P &= A_0 \cdot B_0 \cdot \left(1 \pm \left(\frac{\Delta A}{A_0} + \frac{\Delta B}{B_0}\right) \right)
\end{align*}
```
(здесь предполагается, что относительные погрешности величин $A$ и $B$ много меньше единицы, т.е. их произведение мало по сравнению с каждым из сомножителей).

С точки зрения реализации процедур, работающих с результатами измерений, мы можем написать:
```julia
function add_meas(a, b)
	let mean_a = meanval(a), uabs_a = abs_uncert(a),
        mean_b = meanval(b), uabs_b = abs_uncert(b),
		mean_c = mean_a + mean_b, uabs_c = uabs_a + uabs_b
		return meas_from_ave_abs(mean_c, uabs_c)
	end
end

function sub_meas(a, b)
	let mean_a = meanval(a), uabs_a = abs_uncert(a),
        mean_b = meanval(b), uabs_b = abs_uncert(b),
		mean_c = mean_a - mean_b, uabs_c = uabs_a + uabs_b
		return meas_from_ave_abs(mean_c, uabs_c)
	end
end

function mul_meas(a, b)
	let mean_a = meanval(a), urel_a = rel_uncert(a),
        mean_b = meanval(b), urel_b = rel_uncert(b),
		mean_c = mean_a * mean_b, urel_c = urel_a + urel_b
		return meas_from_ave_rel(mean_c, urel_c)
	end
end

function div_meas(a, b)
	let mean_a = meanval(a), urel_a = rel_uncert(a),
        mean_b = meanval(b), urel_b = rel_uncert(b),
		mean_c = mean_a / mean_b, urel_c = urel_a + urel_b
		return meas_from_ave_rel(mean_c, urel_c)
	end
end
```

Что касается представления чисел, предположим, что им занимаются два разных программиста. Один решил хранить неопределённое значение в виде интервала между минимальным и максимальным значениями, а другой - в виде средней величины и относительной погрешности. У первого, таким образом, реализация выглядит так:
```julia
struct Measurement
	minval
	maxval
end

meanval(a) = (a.maxval + a.minval) / 2
abs_uncert(a) = (a.maxval - a.minval) / 2
rel_uncert(a) = abs_uncert(a) / meanval(a)

meas_from_ave_abs(ave, uabs) = Measurement(ave - uabs, ave + uabs)
meas_from_ave_rel(ave, urel) = meas_from_ave_abs(ave, urel * ave)
```
У второго, соответственно,
```julia
struct Measurement
	ave
	rel_uncert
end

meanval(a) = a.ave
abs_uncert(a) = meanval(a) * rel_uncert(a)
rel_uncert(a) = a.rel_uncert

meas_from_ave_abs(ave, uabs) = meas_from_ave_rel(ave, uabs / ave)
meas_from_ave_rel(ave, urel) = Measurement(ave, urel)
```
"""

# ╔═╡ 81cb4922-25e2-11eb-1bb1-618637c13654
begin
	function add_meas(a, b)
		let mean_a = meanval(a), uabs_a = abs_uncert(a),
			mean_b = meanval(b), uabs_b = abs_uncert(b),
			mean_c = mean_a + mean_b, uabs_c = uabs_a + uabs_b
			return meas_from_ave_abs(mean_c, uabs_c)
		end
	end

	function sub_meas(a, b)
		let mean_a = meanval(a), uabs_a = abs_uncert(a),
			mean_b = meanval(b), uabs_b = abs_uncert(b),
			mean_c = mean_a - mean_b, uabs_c = uabs_a + uabs_b
			return meas_from_ave_abs(mean_c, uabs_c)
		end
	end

	function mul_meas(a, b)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_b = meanval(b), urel_b = rel_uncert(b),
			mean_c = mean_a * mean_b, urel_c = urel_a + urel_b
			return meas_from_ave_rel(mean_c, urel_c)
		end
	end

	function div_meas(a, b)
		let mean_a = meanval(a), urel_a = rel_uncert(a),
			mean_b = meanval(b), urel_b = rel_uncert(b),
			mean_c = mean_a / mean_b, urel_c = urel_a + urel_b
			return meas_from_ave_rel(mean_c, urel_c)
		end
	end
end

# ╔═╡ 6142c686-2534-11eb-35ed-39818bcdc490
md"""
## Модули

Как видим, оба программиста для своих представлений определили структуры данных и набор конструкторов и селекторов с одинаковыми именами. В реальности подобная коллизия имён может случаться не только в программах, реализующих разные решения одной и той же задачи, но и в довольно далёких по смыслу программах. К примеру, процедура `hours(x)` в одной программе может получать количество часов из структуры данных, хранящей время, а в другой переводить секунды в часы.

Программа-клиент в нашем случае может выбрать любую из реализаций хранения величины и её погрешности, но только одну. Удобно, однако, чтобы в системе могли сосуществовать обе реализации, а выбор между ними был максимально удобным. Такая возможность - одна из причин создания *модулей* в языках программирования.

Модуль в Julia - это *пространство имён*, которое при этом само является объектом и имеет имя. Две процедуры или структуры данных с одинаковым названием вполне могут сосуществовать в рамках одной программной системы, если они находятся в разных модулях.
"""

# ╔═╡ a85df2be-25d6-11eb-3bd6-fdcb0a6e3ff9
module MeasSpan

struct Measurement
    minval
    maxval
end

meanval(a) = (a.maxval + a.minval) / 2
abs_uncert(a) = (a.maxval - a.minval) / 2
rel_uncert(a) = abs_uncert(a) / meanval(a)

meas_from_ave_abs(ave, uabs) = Measurement(ave - uabs, ave + uabs)
meas_from_ave_rel(ave, urel) = meas_from_ave_abs(ave, urel * ave)

end # module

# ╔═╡ bec33320-25d6-11eb-1e1f-9b85dba28d0a
module MeasAveRelUncert

struct Measurement
    ave
    rel_uncert
end

meanval(a) = a.ave
abs_uncert(a) = meanval(a) * rel_uncert(a)
rel_uncert(a) = a.rel_uncert

meas_from_ave_abs(ave, uabs) = meas_from_ave_rel(ave, uabs / ave)
meas_from_ave_rel(ave, urel) = Measurement(ave, urel)

end # module

# ╔═╡ d3f305e0-25d6-11eb-01c4-8b9829a06c39
md"""
Теперь, обращаясь к первой или второй реализации, мы можем указывать, из какого модуля берутся определения структур и процедур, записав `ModuleName.identifier`:
"""

# ╔═╡ 978201e6-25d7-11eb-0257-017110dd3248
meas1 = MeasSpan.meas_from_ave_rel(100, 0.01)

# ╔═╡ bf200520-25d7-11eb-3054-438f615588b7
meas2 = MeasAveRelUncert.meas_from_ave_rel(100, 0.01)

# ╔═╡ ba456736-25d7-11eb-1177-0982fc9b7599
md"""
В этом смысле введение модулей можно считать просто методом автоматического введения префиксов для имён внутри модуля, но на этом история не заканчивается. Имена из модуля можно *импортировать* в текущее пространство имён, и тогда к ним можно обращаться уже без префикса:
"""

# ╔═╡ 4f12af10-25e5-11eb-3a09-61c8575f5ac8
md"""
В данном случае точка в `using .MeasSpan` означает, что имя `MeasSpan` ищется внутри пространства имён текущего рабочего модуля (при старте системы создаётся "рабочий" модуль, внутри которого мы определили `MeasSpan` и `MeasAveRelUncert`). Также есть "системные" модули, имена которых не требуют точки перед собой. Подробнее о модулях в Julia: <https://docs.julialang.org/en/v1/manual/modules/>.

Теперь идентификаторы `Measurement`, `meanval` и т.д. импортированы в текущий модуль, и для их использования не нужен префикс. Определения для этих имён взяты из модуля `MeasSpan`. Можно также написать `using .MeasAveRelUncert: ...`, тогда будут взяты альтернативные определения из другого модуля.
"""

# ╔═╡ d6fee0cc-25d8-11eb-1782-2b09211b306c
resistance = meas_from_ave_rel(100, 0.01)

# ╔═╡ a6ed0684-25e2-11eb-0804-d721d36bcf50
voltage = meas_from_ave_abs(230, 20)

# ╔═╡ d70b8eee-25e2-11eb-13f4-07f96a4eb5bf
current = div_meas(voltage, resistance)

# ╔═╡ e0c0fcee-25e2-11eb-08fa-df5ba6a0593b
abs_uncert(current)

# ╔═╡ ea0b0b8c-25e2-11eb-332a-93f3b6113356
meanval(current)

# ╔═╡ 8632daca-2850-11eb-1753-0d30957b5be6
md"""
Для удобства можно указать, какие имена из модуля будут импортироваться по умолчанию. К примеру, один из определённых модулей может выглядеть так:
```julia
module MeasSpan

export Measurement, meanval, abs_uncert, rel_uncert, meas_from_ave_abs, meas_from_ave_rel

struct Measurement
    minval
    maxval
end

meanval(a) = (a.maxval + a.minval) / 2
abs_uncert(a) = (a.maxval - a.minval) / 2
rel_uncert(a) = abs_uncert(a) / meanval(a)

meas_from_ave_abs(ave, uabs) = Measurement(ave - uabs, ave + uabs)
meas_from_ave_rel(ave, urel) = meas_from_ave_abs(ave, urel * ave)

end # module
```

Чтобы добавить в пространство имён текущего модуля все экспортированные имена, достаточно написать
```julia
using .MeasSpan
```

"Встроенные" процедуры, которыми мы пользуемся, расположены в модуле `Base`, который импортируется автоматически при старте системы (т.е. любая программа неявно содержит `using Base`). Это позволяет, например, использовать в процедурах в качестве имён переменных имена из стандартной библиотеки и одновременно эти объекты стандартной библиотеки:
"""

# ╔═╡ 846906ea-2852-11eb-3b2e-8953da6502ca
function sum_cosines_1(a, b)
	let cos = cos(a)
		cos + cos(b)
	end
end

# ╔═╡ aaee04aa-2852-11eb-36da-63893d432850
sum_cosines_1(0, 0)

# ╔═╡ be89a244-2852-11eb-02ac-7585c05760f4
function sum_cosines_2(a, b)
	let cos = cos(a)
		cos + Base.cos(b)
	end
end

# ╔═╡ e6fe7038-2852-11eb-15ff-1f8ebdaf62d8
sum_cosines_2(0, 0)

# ╔═╡ 42706e0e-2c44-11eb-1d83-63b1e9288855
md"""
В заключение, таким образом, можно отметить следующее:
* "Одни и те же данные" могут иметь разные представления
* Определения конструкторов и селекторов для разных представлений должны быть разными, но имена - одинаковыми, чтобы на основе этих имён строить барьеры абстракции
* Процедуры с разными определениями, но одинаковыми именами могут сосуществовать в одной программе, если их определить в разных модулях
* Из модуля можно импортировать имена, чтобы пользоваться ими без явного указания имени модуля
"""

# ╔═╡ Cell order:
# ╟─aacbf2f8-252c-11eb-099e-35718cca5ebb
# ╠═81cb4922-25e2-11eb-1bb1-618637c13654
# ╟─6142c686-2534-11eb-35ed-39818bcdc490
# ╠═a85df2be-25d6-11eb-3bd6-fdcb0a6e3ff9
# ╠═bec33320-25d6-11eb-1e1f-9b85dba28d0a
# ╟─d3f305e0-25d6-11eb-01c4-8b9829a06c39
# ╠═978201e6-25d7-11eb-0257-017110dd3248
# ╠═bf200520-25d7-11eb-3054-438f615588b7
# ╟─ba456736-25d7-11eb-1177-0982fc9b7599
# ╠═b3516a6e-25d8-11eb-1a72-c37f17a6be76
# ╟─4f12af10-25e5-11eb-3a09-61c8575f5ac8
# ╠═d6fee0cc-25d8-11eb-1782-2b09211b306c
# ╠═a6ed0684-25e2-11eb-0804-d721d36bcf50
# ╠═d70b8eee-25e2-11eb-13f4-07f96a4eb5bf
# ╠═e0c0fcee-25e2-11eb-08fa-df5ba6a0593b
# ╠═ea0b0b8c-25e2-11eb-332a-93f3b6113356
# ╟─8632daca-2850-11eb-1753-0d30957b5be6
# ╠═846906ea-2852-11eb-3b2e-8953da6502ca
# ╠═aaee04aa-2852-11eb-36da-63893d432850
# ╠═be89a244-2852-11eb-02ac-7585c05760f4
# ╠═e6fe7038-2852-11eb-15ff-1f8ebdaf62d8
# ╟─42706e0e-2c44-11eb-1d83-63b1e9288855
